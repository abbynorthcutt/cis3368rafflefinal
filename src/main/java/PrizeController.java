import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PrizeController {

    @FXML
    private TextField prizeNameText;
    @FXML
    private TextField prizeSponsText;
    @FXML
    private TextArea prizeDescText;
    @FXML
    private TableColumn<Prize, Integer> prizeIDCol;
    @FXML
    private TableColumn<Prize, String> prizeNameCol;
    @FXML
    private TableColumn<Prize, String> prizeDescCol;
    @FXML
    private TableColumn<Prize, String> prizeSponsCol;
    @FXML
    private TableColumn<Prize, String> prizeWinCol;
    @FXML
    private TableView<Prize> prizeTable;

    public void initialize() throws ClassNotFoundException, SQLException, IOException {
        Connection connection = DatabaseManager.getConnection();
        prizeIDCol.setCellValueFactory(new PropertyValueFactory<Prize,Integer>("prizeName"));
        prizeNameCol.setCellValueFactory(new PropertyValueFactory<Prize,String>("prizeName"));
        prizeDescCol.setCellValueFactory(new PropertyValueFactory<Prize,String>("prizeDesc"));
        prizeSponsCol.setCellValueFactory(new PropertyValueFactory<Prize,String>("prizeSponsor"));
        prizeWinCol.setCellValueFactory(new PropertyValueFactory<Prize, String>("prizeWinner"));
        String query = "SELECT prize.prizeID, prize.prizeName,prize.prizeDesc,prize.prizeSponsor,prize.prizeWinner FROM prize ORDER BY prize.prizeid";
        List<Prize> prize = new ArrayList<>();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Prize prizes = new Prize();
            prizes.setPrizeID(resultSet.getInt(1));
            prizes.setPrizeName(resultSet.getString(2));
            prizes.setPrizeDesc(resultSet.getString(3));
            prizes.setPrizeSponsor(resultSet.getString(4));
            prizes.setPrizeWinner(resultSet.getString(5));
            prize.add(prizes);
        }

        prizeTable.setItems(FXCollections.observableArrayList(prize));

    }

    public void btnAddClicked (ActionEvent actionEvent) throws ClassNotFoundException, IOException, SQLException {
        Connection connection = DatabaseManager.getConnection();
        String nameValue = prizeNameText.getText();
        String descValue = prizeDescText.getText();
        String sponsValue = prizeSponsText.getText();
        String query ="INSERT INTO prize(prizename, prizedesc, prizesponsor) VALUES (?, ?, ?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, nameValue);
            preparedStatement.setString(2, descValue);
            preparedStatement.setString(3, sponsValue);
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }
        initialize();

    }

    public void btnUpdateClicked (ActionEvent actionEvent) throws SQLException, IOException, ClassNotFoundException {
        Connection connection = DatabaseManager.getConnection();
        Prize prizeID = prizeTable.getSelectionModel().getSelectedItem();
        int id = prizeID.getPrizeID();
        String nameValue = prizeNameText.getText();
        String descValue = prizeDescText.getText();
        String sponsValue = prizeSponsText.getText();
        String query ="UPDATE prize SET prizename=?, prizedesc=?, prizesponsor=? WHERE prizeid="+id;
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, nameValue);
            preparedStatement.setString(2, descValue);
            preparedStatement.setString(3, sponsValue);
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }
        initialize();

    }

    public void btnDeleteClicked (ActionEvent actionEvent) throws SQLException, ClassNotFoundException {
        Connection connection = DatabaseManager.getConnection();
        Prize prizeID = prizeTable.getSelectionModel().getSelectedItem();
        int id = prizeID.getPrizeID();
        String sql = "DELETE FROM prize WHERE prizeid ="+id;
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.execute();
        prizeTable.getItems().remove(prizeID);
        preparedStatement.close();
        connection.close();

    }



    public void btnReturnClicked (ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("main.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

    }

}
