import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomizeController {

    @FXML
    private TableColumn<Prize, Integer> prizeIDCol;
    @FXML
    private TableColumn<Prize, String> prizeNameCol;
    @FXML
    private TableColumn<Prize, String> prizeDescCol;
    @FXML
    private TableColumn<Prize, String> prizeSponsCol;
    @FXML
    private TableColumn<Prize, String> prizeWinnerCol;
    @FXML
    private TableView<Prize> prizeTable;
    @FXML
    private TextField winnerText;


    public void initialize() throws ClassNotFoundException, SQLException, IOException {
        Connection connection = DatabaseManager.getConnection();
        prizeIDCol.setCellValueFactory(new PropertyValueFactory<Prize,Integer>("prizeID"));
        prizeNameCol.setCellValueFactory(new PropertyValueFactory<Prize,String>("prizeName"));
        prizeDescCol.setCellValueFactory(new PropertyValueFactory<Prize,String>("prizeDesc"));
        prizeSponsCol.setCellValueFactory(new PropertyValueFactory<Prize,String>("prizeSponsor"));
        prizeWinnerCol.setCellValueFactory(new PropertyValueFactory<Prize, String>("prizeWinner"));
        String query = "SELECT prize.prizeID, prize.prizeName,prize.prizeDesc,prize.prizeSponsor,prize.prizeWinner FROM prize ORDER BY prize.prizeid";
        List<Prize> prize = new ArrayList<>();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Prize prizes = new Prize();
            prizes.setPrizeID(resultSet.getInt(1));
            prizes.setPrizeName(resultSet.getString(2));
            prizes.setPrizeDesc(resultSet.getString(3));
            prizes.setPrizeSponsor(resultSet.getString(4));
            prizes.setPrizeWinner(resultSet.getString(5));
            prize.add(prizes);
        }

        prizeTable.setItems(FXCollections.observableArrayList(prize));

    }



    public void btnGetWinClicked (ActionEvent actionEvent) throws ClassNotFoundException, SQLException {
        Connection connection = DatabaseManager.getConnection();
        String query = "SELECT participantname FROM participant WHERE participantAbsent=0 AND participantWin=0";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();
        List<String> winners = new ArrayList<>();
        while (resultSet.next()){
            winners.add(resultSet.getString(1));
        }
        Random random = new Random();
        String winner = winners.get(random.nextInt(winners.size()));
        winnerText.setText(winner);




    }


    public void btnAbsentClicked (ActionEvent actionEvent) throws ClassNotFoundException, SQLException {
        Connection connection = DatabaseManager.getConnection();
        String participantName = winnerText.getText();
        String query = "UPDATE participant SET participantAbsent=1 WHERE participantName=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1,participantName);
        preparedStatement.execute();
        preparedStatement.close();
        connection.close();



    }

    public void btnSaveClicked (ActionEvent actionEvent) throws ClassNotFoundException, SQLException, IOException {
        Connection connection = DatabaseManager.getConnection();
        String participantName = winnerText.getText();
        Prize prizeID = prizeTable.getSelectionModel().getSelectedItem();
        int id = prizeID.getPrizeID();
        String query = "UPDATE prize SET prizewinner=? WHERE prizeID="+id;
        String query2 = "UPDATE participant SET participantWin=1 WHERE participantName=?";
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        preparedStatement.setString(1,participantName);
        preparedStatement.execute();
        PreparedStatement preparedStatement2 = connection.prepareStatement(query2);
        preparedStatement2.setString(1,participantName);
        preparedStatement2.execute();
        preparedStatement.close();
        preparedStatement2.close();
        connection.close();
        initialize();


    }

    public void btnReturnClicked (ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("main.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(root));
        primaryStage.show();

    }

}
