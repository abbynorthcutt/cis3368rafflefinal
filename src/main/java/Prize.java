public class Prize {

    private int prizeID;
    private String prizeName;
    private String prizeDesc;
    private String prizeSponsor;
    private String prizeWinner;

    @Override
    public String toString() {
        return "Prize{" +
                "prizeID=" + prizeID +
                ", prizeName='" + prizeName + '\'' +
                ", prizeDesc='" + prizeDesc + '\'' +
                ", prizeSponsor='" + prizeSponsor + '\'' +
                ", prizeWinner='" + prizeWinner + '\'' +
                '}';
    }

    public int getPrizeID() {
        return prizeID;
    }

    public void setPrizeID(int prizeID) {
        this.prizeID = prizeID;
    }

    public String getPrizeName() {
        return prizeName;
    }

    public void setPrizeName(String prizeName) {
        this.prizeName = prizeName;
    }

    public String getPrizeDesc() {
        return prizeDesc;
    }

    public void setPrizeDesc(String prizeDesc) {
        this.prizeDesc = prizeDesc;
    }

    public String getPrizeSponsor() {
        return prizeSponsor;
    }

    public void setPrizeSponsor(String prizeSponsor) {
        this.prizeSponsor = prizeSponsor;
    }

    public String getPrizeWinner() {
        return prizeWinner;
    }

    public void setPrizeWinner(String prizeWinner) {
        this.prizeWinner = prizeWinner;
    }
}
