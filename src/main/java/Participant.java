public class Participant {

    private int participantID;
    private String participantName;
    private String participantEmail;
    private String participantPhone;
    private String participantCompany;
    private String participantTitle;

    @Override
    public String toString() {
        return "Participant{" +
                "participantID=" + participantID +
                ", participantName='" + participantName + '\'' +
                ", participantEmail='" + participantEmail + '\'' +
                ", participantPhone='" + participantPhone + '\'' +
                ", participantCompany='" + participantCompany + '\'' +
                ", participantTitle='" + participantTitle + '\'' +
                '}';
    }

    public int getParticipantID() {
        return participantID;
    }

    public void setParticipantID(int participantID) {
        this.participantID = participantID;
    }

    public String getParticipantName() {
        return participantName;
    }

    public void setParticipantName(String participantName) {
        this.participantName = participantName;
    }

    public String getParticipantEmail() {
        return participantEmail;
    }

    public void setParticipantEmail(String participantEmail) {
        this.participantEmail = participantEmail;
    }

    public String getParticipantPhone() {
        return participantPhone;
    }

    public void setParticipantPhone(String participantPhone) {
        this.participantPhone = participantPhone;
    }

    public String getParticipantCompany() {
        return participantCompany;
    }

    public void setParticipantCompany(String participantCompany) {
        this.participantCompany = participantCompany;
    }

    public String getParticipantTitle() {
        return participantTitle;
    }

    public void setParticipantTitle(String participantTitle) {
        this.participantTitle = participantTitle;
    }
}
