public class Sponsor {

    private int sponsorID;
    private String companyName;
    private String repName;
    private String repEmail;
    private String repPhone;
    private String repTitle;

    @Override
    public String toString() {
        return "Sponsor{" +
                "sponsorID=" + sponsorID +
                ", companyName='" + companyName + '\'' +
                ", repName='" + repName + '\'' +
                ", repEmail='" + repEmail + '\'' +
                ", repPhone='" + repPhone + '\'' +
                ", repTitle='" + repTitle + '\'' +
                '}';
    }

    public int getSponsorID() {
        return sponsorID;
    }

    public void setSponsorID(int sponsorID) {
        this.sponsorID = sponsorID;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getRepName() {
        return repName;
    }

    public void setRepName(String repName) {
        this.repName = repName;
    }

    public String getRepEmail() {
        return repEmail;
    }

    public void setRepEmail(String repEmail) {
        this.repEmail = repEmail;
    }

    public String getRepPhone() {
        return repPhone;
    }

    public void setRepPhone(String repPhone) {
        this.repPhone = repPhone;
    }

    public String getRepTitle() {
        return repTitle;
    }

    public void setRepTitle(String repTitle) {
        this.repTitle = repTitle;
    }
}
