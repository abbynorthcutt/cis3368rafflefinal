import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ParticipantController {

    @FXML
    private TextField partNameText;
    @FXML
    private TextField partEmailText;
    @FXML
    private TextField partPhoneText;
    @FXML
    private TextField partCompText;
    @FXML
    private TextField partTitleText;
    @FXML
    private TableColumn<Participant, Integer> partIDCol;
    @FXML
    private TableColumn<Participant, String> partNameCol;
    @FXML
    private TableColumn<Participant, String> partEmailCol;
    @FXML
    private TableColumn<Participant, String> partPhoneCol;
    @FXML
    private TableColumn<Participant, String> partCompanyCol;
    @FXML
    private TableColumn<Participant, String> partTitleCol;
    @FXML
    private TableView<Participant> partTable;

    public void initialize() throws ClassNotFoundException, SQLException, IOException {
        Connection connection = DatabaseManager.getConnection();
        partIDCol.setCellValueFactory(new PropertyValueFactory<Participant,Integer>("participantID"));
        partNameCol.setCellValueFactory(new PropertyValueFactory<Participant,String>("participantName"));
        partEmailCol.setCellValueFactory(new PropertyValueFactory<Participant,String>("participantEmail"));
        partPhoneCol.setCellValueFactory(new PropertyValueFactory<Participant,String>("participantPhone"));
        partCompanyCol.setCellValueFactory(new PropertyValueFactory<Participant, String>("participantCompany"));
        partTitleCol.setCellValueFactory(new PropertyValueFactory<Participant, String>("participantTitle"));
        String query = "SELECT participant.participantid,participant.participantName,participant.participantEmail,participant.participantPhone,participant.participantCompany,participant.participantTitle FROM participant ORDER BY participant.participantid";
        List<Participant> participant = new ArrayList<>();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Participant participants = new Participant();
            participants.setParticipantID(resultSet.getInt(1));
            participants.setParticipantName(resultSet.getString(2));
            participants.setParticipantEmail(resultSet.getString(3));
            participants.setParticipantPhone(resultSet.getString(4));
            participants.setParticipantCompany(resultSet.getString(5));
            participants.setParticipantTitle(resultSet.getString(6));
            participant.add(participants);
        }

        partTable.setItems(FXCollections.observableArrayList(participant));

    }



    public void btnAddClicked (ActionEvent actionEvent) throws ClassNotFoundException, IOException, SQLException {
        Connection connection = DatabaseManager.getConnection();
        String nameValue = partNameText.getText();
        String emailValue = partEmailText.getText();
        String phoneValue = partPhoneText.getText();
        String companyValue = partCompText.getText();
        String titleValue = partTitleText.getText();
        String query ="INSERT INTO participant(participantname, participantemail, participantphone, participantcompany, participanttitle) VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, nameValue);
            preparedStatement.setString(2, emailValue);
            preparedStatement.setString(3, phoneValue);
            preparedStatement.setString(4, companyValue);
            preparedStatement.setString(5, titleValue);
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }
        initialize();

    }

    public void partTableClicked (MouseEvent mouseEvent) {
        Participant participant = partTable.getSelectionModel().getSelectedItem();
        String partName = participant.getParticipantName();
        String partEmail = participant.getParticipantEmail();
        String partPhone = participant.getParticipantPhone();
        String partCompany = participant.getParticipantCompany();
        String partTitle = participant.getParticipantTitle();
        partNameText.setText(partName);
        partEmailText.setText(partEmail);
        partPhoneText.setText(partPhone);
        partCompText.setText(partCompany);
        partTitleText.setText(partTitle);
    }

    public void btnUpdateClicked (ActionEvent actionEvent) throws ClassNotFoundException, IOException, SQLException {
        Connection connection = DatabaseManager.getConnection();
        Participant participant = partTable.getSelectionModel().getSelectedItem();
        int id = participant.getParticipantID();
        String nameValue = partNameText.getText();
        String emailValue = partEmailText.getText();
        String phoneValue = partPhoneText.getText();
        String companyValue = partCompText.getText();
        String titleValue = partTitleText.getText();
        String query ="UPDATE participant SET participantname = ?, participantemail = ?, participantphone = ?, participantcompany = ?, participanttitle = ? WHERE participantid = "+id;
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, nameValue);
            preparedStatement.setString(2, emailValue);
            preparedStatement.setString(3, phoneValue);
            preparedStatement.setString(4, companyValue);
            preparedStatement.setString(5, titleValue);
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }

        initialize();

    }

    public void btnDeleteClicked (ActionEvent actionEvent) throws ClassNotFoundException, SQLException {
        Connection connection = DatabaseManager.getConnection();
        Participant participantID = partTable.getSelectionModel().getSelectedItem();
        int id = participantID.getParticipantID();
        String sql = "DELETE FROM participant WHERE participantid ="+id;
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.execute();
        partTable.getItems().remove(participantID);
        preparedStatement.close();
        connection.close();

    }

    public void btnReturnClicked (ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("main.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
