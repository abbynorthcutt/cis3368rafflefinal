import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SponsorController {

    @FXML
    private TextField compText;
    @FXML
    private TextField repText;
    @FXML
    private TextField repEmailText;
    @FXML
    private TextField repPhoneText;
    @FXML
    private TextField repTitleText;
    @FXML
    private TableColumn<Sponsor, Integer> sponsIDCol;
    @FXML
    private TableColumn<Sponsor, String> sponsCompCol;
    @FXML
    private TableColumn<Sponsor, String> sponsRepCol;
    @FXML
    private TableColumn<Sponsor, String> sponsEmailCol;
    @FXML
    private TableColumn<Sponsor, String> sponsPhoneCol;
    @FXML
    private TableColumn<Sponsor, String> sponsTitleCol;
    @FXML
    private TableView<Sponsor> sponsTable;


    public void initialize() throws ClassNotFoundException, SQLException, IOException {
        Connection connection = DatabaseManager.getConnection();
        sponsIDCol.setCellValueFactory(new PropertyValueFactory<Sponsor,Integer>("sponsorID"));
        sponsCompCol.setCellValueFactory(new PropertyValueFactory<Sponsor,String>("companyName"));
        sponsRepCol.setCellValueFactory(new PropertyValueFactory<Sponsor,String>("repName"));
        sponsEmailCol.setCellValueFactory(new PropertyValueFactory<Sponsor,String>("repEmail"));
        sponsPhoneCol.setCellValueFactory(new PropertyValueFactory<Sponsor, String>("repPhone"));
        sponsTitleCol.setCellValueFactory(new PropertyValueFactory<Sponsor, String>("repTitle"));
        String query = "SELECT sponsor.sponsorID,sponsor.companyName,sponsor.repName,sponsor.repEmail,sponsor.repPhone,sponsor.repTitle FROM sponsor ORDER BY sponsor.sponsorid";
        List<Sponsor> sponsor = new ArrayList<>();
        PreparedStatement preparedStatement = connection.prepareStatement(query);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {
            Sponsor sponsors = new Sponsor();
            sponsors.setSponsorID(resultSet.getInt(1));
            sponsors.setCompanyName(resultSet.getString(2));
            sponsors.setRepName(resultSet.getString(3));
            sponsors.setRepEmail(resultSet.getString(4));
            sponsors.setRepPhone(resultSet.getString(5));
            sponsors.setRepTitle(resultSet.getString(6));
            sponsor.add(sponsors);
        }

        sponsTable.setItems(FXCollections.observableArrayList(sponsor));

    }

    public void btnAddClicked (ActionEvent actionEvent) throws ClassNotFoundException, IOException, SQLException {
        Connection connection = DatabaseManager.getConnection();
        String compValue = compText.getText();
        String repValue = repText.getText();
        String emailValue = repEmailText.getText();
        String phoneValue = repPhoneText.getText();
        String titleValue = repTitleText.getText();
        String query ="INSERT INTO sponsor(companyname, repname, repemail, repphone, reptitle) VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, compValue);
            preparedStatement.setString(2, repValue);
            preparedStatement.setString(3, emailValue);
            preparedStatement.setString(4, phoneValue);
            preparedStatement.setString(5, titleValue);
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }
        initialize();

    }

    public void partTableClicked (MouseEvent mouseEvent) {
        Sponsor sponsor = sponsTable.getSelectionModel().getSelectedItem();
        String compName = sponsor.getCompanyName();
        String repName = sponsor.getRepName();
        String repEmail = sponsor.getRepEmail();
        String repPhone = sponsor.getRepPhone();
        String repTitle = sponsor.getRepTitle();
        compText.setText(compName);
        repText.setText(repName);
        repEmailText.setText(repEmail);
        repPhoneText.setText(repPhone);
        repTitleText.setText(repTitle);
    }

    public void btnUpdateClicked (ActionEvent actionEvent) throws ClassNotFoundException, IOException, SQLException {
        Connection connection = DatabaseManager.getConnection();
        Sponsor sponsor = sponsTable.getSelectionModel().getSelectedItem();
        int id = sponsor.getSponsorID();
        String compValue = compText.getText();
        String repValue = repText.getText();
        String emailValue = repEmailText.getText();
        String phoneValue = repPhoneText.getText();
        String titleValue = repTitleText.getText();
        String query ="UPDATE sponsor SET companyname=?, repname=?, repemail=?, repphone=?, reptitle=? WHERE sponsorid="+id;
        try (PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, compValue);
            preparedStatement.setString(2, repValue);
            preparedStatement.setString(3, emailValue);
            preparedStatement.setString(4, phoneValue);
            preparedStatement.setString(5, titleValue);
            preparedStatement.executeUpdate();
        }catch (SQLException e) {
            e.printStackTrace();
        }
        initialize();
    }

    public void btnDeleteClicked (ActionEvent actionEvent) throws ClassNotFoundException, SQLException {
        Connection connection = DatabaseManager.getConnection();
        Sponsor sponsorID = sponsTable.getSelectionModel().getSelectedItem();
        int id = sponsorID.getSponsorID();
        String sql = "DELETE FROM sponsor WHERE sponsorid ="+id;
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.execute();
        sponsTable.getItems().remove(sponsorID);
        preparedStatement.close();
        connection.close();

    }

    public void btnReturnClicked (ActionEvent actionEvent) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("main.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }

}
