import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseManager {

    private static String url = "jdbc:postgresql://localhost:5432/raffle";
    private static String user = "postgres";
    private static String password = "postgres";
    private static String driver = "org.postgresql.Driver";
    private static Connection connection;

    public static Connection getConnection() throws ClassNotFoundException {
        try{
            Class.forName(driver);
            connection= DriverManager.getConnection(url,user,password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }


}
